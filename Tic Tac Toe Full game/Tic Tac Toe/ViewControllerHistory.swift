//
//  ViewControllerHistory.swift
//  Tic Tac Toe
//
//  Created by clicklabs92 on 10/02/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit
var record:[String] = [""]

class ViewControllerHistory: UIViewController,UITableViewDelegate {

   @IBOutlet weak var historyBoard: UITableView!
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->Int {
        return record.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->UITableViewCell {
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.sizeToFit()
        cell.textLabel?.numberOfLines = 5
        cell.textLabel?.text = record[indexPath.row]
        return cell
    }
    
    override func viewWillAppear(animated: Bool) { //setting the initial data load
        record = []
        if var itemInMemory :AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("record"){
            for var i = 0 ; i < itemInMemory.count; ++i {
                record.append(itemInMemory[i] as NSString)
            }
        }
        historyBoard.reloadData()
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // function for deleting the todo item
        if editingStyle == UITableViewCellEditingStyle.Delete {
            record.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            historyBoard.reloadData()
            let storedItem = record
            NSUserDefaults.standardUserDefaults().setObject(storedItem, forKey: "record")
            NSUserDefaults.standardUserDefaults().synchronize()
            }
        }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //return the table height
        return tableView.rowHeight;
    }

     override func viewDidLoad() {
        super.viewDidLoad()
                // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
