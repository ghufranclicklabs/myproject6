//
//  ViewControllerFirst.swift
//  Tic Tac Toe
//
//  Created by clicklabs92 on 04/02/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerFirst: UIViewController, UIPickerViewDelegate {
    
    //label Field
    @IBOutlet weak var playerOneLabel: UILabel!
    @IBOutlet weak var playerTwoLabel: UILabel!
    //text field
    @IBOutlet weak var playerOneTextField: UITextField!
    @IBOutlet weak var playerTwoTextField: UITextField!
    //picker image label
    @IBOutlet weak var pickCrossSignPicker: UIPickerView!
    @IBOutlet weak var pickNoughtsSignPicker: UIPickerView!
    var selectCrossIcon = "cross1.png"
    var selectNoughtsIcon = "not1.png"
    let noughtsPickerData = ["not1.png","not2.png","not3.png","not4.png","not5.png"]
    let crossPickerData = ["cross1.png","cross2.png","cross3.png","cross4.png","cross5.png"]
    
    //function for first player choose icon
    @IBAction func playerOneChoosesignButton(sender: AnyObject) {
        pickNoughtsSignPicker.hidden = false
        pickCrossSignPicker.hidden = true
        self.playerOneTextField.endEditing(true)
    }
    // fuction for second player choose icon
    @IBAction func playerTwoChooseSignButton(sender: AnyObject) {
      pickCrossSignPicker.hidden = false
        pickNoughtsSignPicker.hidden = true
        self.playerTwoTextField.endEditing(true)
    }
    // number of components in picker
    func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int {
        return 1
    }
    // number of rows in picker
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return noughtsPickerData.count
    }
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var myView = UIView(frame: CGRectMake(0, 0, pickerView.bounds.width - 30, 30))
        var myImageView = UIImageView(frame: CGRectMake(0, 0, 30, 20))
        if pickerView.tag == 1 {
            var image = crossPickerData[row]
            myImageView.image = UIImage(named: "\(image)")
            myView.addSubview(myImageView)
        }else if pickerView.tag == 2 {
            var image = noughtsPickerData[row]
            myImageView.image = UIImage(named: "\(image)")
            myView.addSubview(myImageView)
        }
        return myView
    }// picker contains a view , view contains UIImageview , and UIImageview is assigned is UIImage based on picker tag
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            selectCrossIcon = crossPickerData[row]
        } else {
            selectNoughtsIcon = noughtsPickerData[row]
        }
    }// on selecting picker row based on picker tag
   
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       if sender?.tag != 230 {
           var gamePlayClassObject = segue.destinationViewController as ViewController
        playerOneNameGlobal = playerOneTextField.text
        playerTwoNameGlobal = playerTwoTextField.text
      }//passing values to ViewController
    }
    //play button action
    @IBAction func playButton(sender: AnyObject) {
        playerOneNameGlobal = playerOneTextField.text
        playerTwoNameGlobal = playerTwoTextField.text
        playerOneTextField.text = ""
        playerTwoTextField.text = ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
               // Do any additional setup after loading the view.
     }
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
